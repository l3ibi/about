# image
FROM python:latest

LABEL version="0.2" maintener="l3ibi l3ibi@cametleon.me"

ARG APT_FLAGS="-q -y"
ARG DOCROOT="/var/www/html/"
ARG VERBOSE=true

RUN python3 -m pip install pelican[markdown]

# Run Nginx
RUN apt update -y && apt-get upgrade ${APT_FLAGS} \
&& apt install ${APT_FLAGS} nginx


COPY ./ ./

# Generate output
RUN pelican ./content -o ${DOCROOT} -s publishconf.py

ENTRYPOINT nginx -g 'daemon off;'

# build
expose 80
