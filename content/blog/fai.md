Title: Ça te dit, on monte un FAI ?
Category: Blog
Tags: FAI
Slug: makefai
Lang: fr
Status: published
Summary: Comment on se lance dans la création d’un Fournisseur d’Accès Internet ?

*«Salut Phhilou, je reviens de l’AG de la FFDN [ref] Fédération des fournisseurs
d’accès internet[/ref] c’était trop cool. Ça te
dirait pas qu’on en monte un dans le coin ? »*

Voilà à peu près comment démarre l’histoire.
Phhilou a répondu oui.

Un an
plus tard toujours rien. On avait pourtant commencé à bosser, on avait regardé
la conférence de ju «[Internet is coming](https://julien.vaubourg.com/)», prit des notes, commencé à réfléchir… 

Un an plus tard je re-reviens de l’AG, j’ai rencontré plein de camarades
prêt·e·s à nous filer un coup de main. Rebelotte, «Phhilou, toujours ok pour
le FAI ? — Oui. »
Ne manquait plus que l’occasion, savoir
par où commencer, savoir qu’est-ce qu’on veut faire. 


C’est alors qu’on entend parler d’un évènement, dans une ancienne villa d’armateurs, sur les
hauteurs de Saint-Brieuc. Un évènement qui durera un weekend,  6 mois plus tard.
On se rend à la réunion d’info, nous supposions que leur accès internet ne
serait pas tip-top, et nous proposons de nous occuper du réseau. 

Ayant tanné un certain nombre de camarade de la fédé[ref]Le petit nom de la
FFDN[/ref], je savais qu’il nous
fallait un point d’accès qui dépote, puis joindre la villa par des antennes
wifi, il faudrait mettre en place un tunnel vpn [ref]Un tunnel chiffré qui présente l’avantage de rendre aveugle le
fournisseur d’accès internet depuis lequel on est connecté (en gros), de
nettoyer la connection, et d’avoir une ip fixe, fournit par le fai associatif.[/ref] chez une asso de la fédé.


Voilà concrètement avec quelles connaissances on est parti. Nous voilà partis à
commander des antennes, à inviter les quelques camarades qui veulent passer
nous voir, pour nous aider à monter le bousin.

Les antennes, n’arrivent pas, on les pense perdues, on nous dit qu’elles ont
été retrouvées, puis en fait non… du coup pas d’antennes pour faire des tests,
je passe chez des camarades qui en avait une paire et qui ne s’en serve pas
pour le moment. Donc nous voilà avec nos maigres connaissances et deux
antennes.


Un camarade d’Aquilenet passe à la maison, c’est l’occasion de tisser des
liens, de discuter la faisabilité de la chose, dans la foulée, il contacte
Aquilenet pour savoir s’ils peuvent nous mettre à dispo un vpn, accord de
principe.

On part ensemble à Nantes voir FAIMaison, un autre FAI associatif à Nantes, où
on va passer le weekend à aller à la plage, manger des galettes, et faire de
la maintenance sur un réseau. De retour à la maison, je sais sertir du
RJ45, et j’ai bien envie de monter sur d’autre toits.


Voilà comment ça a commencé.


**Prochaine étape :** rédaction des status, et
officialisation de l’asso. 

---
