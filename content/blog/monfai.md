Title: Je suis abonné à mon FAI
Category: Blog
Status: draft
Tags: FAI, Asso, FAIbreizh
Slug: mon-fai
Authors: l3ibi
Summary: Réflexions autour du fait d'être abonné au fournisseurs d'accès
internet que j'ai contribué à créér.

# Rappel
Comme évoqué dans [On l'a fait]({filename}./onlafait.md) nous avoins avec
quelques ami·e·s créé un fournisseur d'accès internet. 

Me voici donc abonné à mon propre FAI aka [FAIbreizh](https://fai.bzh). C'est
un chose assez rigolote car c'est également la première ligne à mon nom.
Ainsi, mon premier abonnement internet e
