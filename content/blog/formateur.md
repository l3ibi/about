Title: Formateur dans une GEN
Tags: formation, experience, perso, 
Categorie: Blog
lang: fr 
slug:formateur
Status: Draft
Summary: Retour d’expérience de formateur dans une «Grande Ecole du
Numérique».

Depuis 2ans je suis formateur dans ce que l’on nomme par le label «Grande
Ecole du Numérique», l’objectif de la formation est de donner à des personnes
en recherche d’emploi des clés pour comprendre les enjeux du numérique, savoir
communiquer sur les réseaux, animer un site internet, et éventuellement en
bricoler un à partir d’un CMS. 

Dans les faits j’ai plutôt les mains libre pour organiser le contenu de mes
journées de formation. Les compétences que **moi** j’attends ne sont pas
toujours les même que mon organisme de tutelle, mais on arrive plutôt bien à
concilier les deux. 
En terme de compétences pour moi, j’attend :
*  Comprendre globalement comment fonctionne internet. 
*  Savoir utiliser internet pour faire des recherches
*  Savoir être autonome (globalement utiliser internet pour faire ce que l’on
    a envie de faire)
*  Connaitre les outils de travail collaboratif
*  Savoir utiliser l’email (savoir trier ses mails, répondre dans le corps
    d’un email, écrire un email…) 
Globalement, et c’est un peu comme ça qu’on me l’a vendue à l’époque, je dois,
en 6mois, leur donner des outils pour «apprendre à apprendre». 

Voilà pour l’introduction. 
Je suis moi même autodidacte et évolue aujourd’hui plutôt dans des cercles
disons, liés à l’informatique, alors que je viens plutôt des sciences humaines
que des sciences technologique.
J’ai aussi un capital social qui fait que je suis plutôt éloigné des milieux
«illectronique»[ref]je découvre le terme[/ref].
