Title: Adresser des IP’s
Category: Blog
Tags: FAI, technique, wireguard, ip
Slug: adresser-ip-s
Date: 2017-02-05
Status: published
Authors: l3ibi
Summary: Explication de texte sur l’adressage d’ip à des adhérent·e·s d’un Fournisseurs d’accès internet.

En tant que Fournisseurs d’Accès Internet associatif [FAIbreizh]({filename}./onlafait.md) souhaite fournir une ip à chacun·e·s de ses abonné·e·s. 

# Partie Administrative
Pour commencer, il nous a fallu créer une association, afin de pouvoir éditer
des factures, ouvir un compte en banque… Il nous fallait également nous
déclarer à l’ARCEP[ref]Autorité de Régulation des Communication Électronique
et des Postes. En gros le gendarme des télécoms)[/ref]. Avec ça c’est bon,
vous êtes opérateur télécom. Voilà pour la partie administrative.

# Obtenir des IP’s
Nous souhaitons, attribuer aux abonné·e·s de FAIbreizh une adresse ip. Pour faire simple une
adresse ip c’est comme un numéro de téléphone, elle vous permet d’utiliser
internet, mais également, si elle est fixe et non « dégradée », d’héberger
chez vous vos services. En général, les FAI que connaient  fournissent une adresse ip qui a tendance à changer dans le temps, et
elle est souvent partagée entre plusieurs abonné·e·s, ce qui limite les possibilités
d’auto-hébergement.

Du coup, chez FAIbreizh on fournira des adresses ip. Sauf, que ces adresses on
les trouves pas au Shopi® du coin, il faut s’adresser à un
LIR[ref]Local Internet Registar[/ref] qui peut être une entreprise, une association,
sans doute même un particulier(sauf que le coût d’entrée pour devenir LIR est
un peu cher). Le LIR, se voit attribuer un pool d’ip qu’il distribue ensuite à
qui en fait la demande, moyennant, généralement une contrepartie financière.

Nous, on a fait la demande à l’association [Gitoyen](https://gitoyen.net/),
qui nous en a fournit une tranche : `80.67.187.0/24` et `2001:913:6000::/36`.

On a également annoncé au monde entier que nous sommes responsable de ce
bloc d’ip grace au RIPE[ref]Réseau IP Européen.[/ref]. Ceci permet, entre
autre, à la justice de savoir à qui s’adresser en cas d’infraction. De son côté, Gitoyen, lui
a indiqué que lui n’en était plus responsable.

En suite il nous a fallut trouver un endroit où accueillir ces ip’s, comme à notre habitude on a choisi de passer par une association :
[Grifon](https://grifon.fr), qui nous fournit une machine virtuelle.

Grifon, de son côté a dit au RIPE que ces ip’s était dans son réseau à lui, et
par un protocole encore un peu obscure pour nous à savoir BGP, dit à ses
voisins de routeurs que ces ip’s sont dans son réseau à lui.

Ainsi, notre machine chez Grifon possède toute les ip’s du range qui nous a été
attribué par Gitoyen.

# Conclusion
Au fond, c’est pas bien compliqué d’être FAI, c’est quelques papiers à signer
quelques contact à avoir.
Le plus compliqué au fond, et de dire au RIPE qu’on est en charge du pool
d’ip. Les écritures sont un peu austère, de notre côté on a globalement suivi
la documentation de nos camarades de
[Baionet](https://wiki.baionet.fr/doku.php?id=ipandas).
