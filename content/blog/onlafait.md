Title: On l’a fait
Category: Blog
Tags: FAI
Slug: on-l-a-fait
Status: published
Summary: Retour d’expérience de la création d’une connection internet pour un évènement à Saint-Brieuc.

*« Franchement on n’aurait jamais dû se lancer là dedans, je sais pas
qu’est-ce qu’on est venu foutre dans ce bordel.»*

C’est comme ça que l’histoire aurait pu terminée. Dans l’[épisode
précédent]({filename}./fai.md) nous nous lancions dans la création d’un Fournisseur
d’Accès Internet Associatif au travers d’un évènement Muséomix, une petite
centaine de personnes, qui remix et crée des œuvres de musée en un weekend. 

On s’était dit alors que ce serait marrant de leur fournir le réseau. 
Quelques semaine avant l’évènement nous avions bien posé nos antennes sur les
toit, l’une sur un lycée (celle qui fournira l’accès) et une antenne sur le
toit de la Villa (celle qui récupère). Nous avions alors prévu large, et 2/3
semaines avant les antennes étaient configurée, subsistait alors quelque
problème de latence dont nous n’avions pas encore trouvé l’origine. Mais
globalement ça fonctionnait, la latence était _pourrite_ mais le débit était
bon.

Comme on voulait faire les choses proprement, sans utiliser à proprement parlé
le réseau du lycée, aquilenet nous avait fournit un vpn, charge à nous de
faire transiter toute les connections de la villa vers le vpn. On… Je m’étais
dit que c’était bête comme chou, étant donné que j’utilisais moi même un vpn
sur mon ordi, c’était une bête commande à taper et c’était bon. 

C’est donc à ce moment là que les choses ce sont gatée. En fait le réseau du
lycée était un peu fachiste (normal pour un lycée vous me direz… mais plus que
je ne l’imaginais). Nos connexions étaient instable, le vpn se lançait une
fois sur 5, on avait pas mal de déconnections, etc.

On s’est aperçu que la config vpn utilisait une url pour attaquer le serveur
de vpn, **sauf** que le résolveur du lycée bloquait cette adresse. Il a donc
fallut écrire les ip’s en dure dans la config.

Notre routeur était alors bien connecté au vpn, qui lui fournissait également
l’adresse d’un résolveur DNS non menteur, non fachiste (celui d’aquilenet).
Il nous fallait ensuite, utiliser le routeur comme pont entre le réseau local,
celui de la Villa Rohannec’h, le vpn et le reste d’internet.

C’est là que les choses se sont encore un peu compliqué, on a joué avec tout
les paramètres d’iptables et autres bidouilles étranges.
C’est sur le canal IRC de la fédération FFDN que nous avons eu la bonne
commande à taper, qui était :
`iptables -t nat -A POSTROUTING -o tun0 -j MASQUERADE`

La configuration du routeur est sur [wiki.fai.bzh](https://wiki.fai.bzh/doku.php?id=wiki:technique:adminsys:dingo)

Ensuite il ne nous restait plus qu’à scripter l’ensemble pour qu’en cas de
reboot on redémarre le vpn, puis qu’on applique les règles iptables. Simple.
Sauf, que je ne sais plus pour quelle raison on avait besoin d’exécuter les
choses dans un certain ordre, et tester que la précédente étape était terminée
pour exécuter la suivante.
Et comme on n’est pas toujours très bon, avec la fatique, le stress, notre
inexpérience on a foiré le script et on c’est rendu compte qu’une de nos
étapes (`openvpn vpn.conf`) s’exécutait bien, mais pas en deamon du coup la seconde étape ne
s’exécutait jamais… voilà voilà.



