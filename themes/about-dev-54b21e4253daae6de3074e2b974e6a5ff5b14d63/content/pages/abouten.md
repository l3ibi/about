Title: About
Date: 2016-07-03 10:20
Autors: Camille
Slug: about
Lang: en
Status: Published

I enjoy...  
=======

* the commande line
* testing new recipes
* learning

Studies:
========

* Master 1 of clinical psychology and social psychology.
* Master 2 of public law mention Ethic, Norm, and Health.

Works (in french): 
===========================


* Concept, limites et bénéfices du secret.([pdf](/pdfs/Lesecret.pdf))
* Le droit à l'indemnisation : nouveau partage entre solidarité et responsabilité.([pdf](/pdfs/Indemnisation.pdf))
* Le rôle de l'agir dans le processus de subjectivation à l'adolescence.
* Les intellos précaires : essai commenté.([pdf](/pdfs/IntellosPrecaires.pdf))
* Les enjeux éthique du tirage au sort dans l'accès au soin. ([pdf](/pdfs/TirageAuSort.pdf))
* Marie Gaille : La valeur de la vie : essai commenté. ([pdf](/pdfs/LaValeurDeLaVie.pdf))

