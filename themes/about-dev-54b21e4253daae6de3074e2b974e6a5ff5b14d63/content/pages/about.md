Title: Présentation 
Date: 2016-07-03 10:20
Authors: Camille
Slug: about
URL:
save_as:index.html
Lang: fr
Status: Published

J'aime :
========

* La ligne de commande
* Essayer de nouvelles recettes
* Apprendre


Etudes :
========

* Master 1 de psychologie clinique pathologique et clinique du lien social.
* Master 2 de droit public mention Ethique, Normes et Santé. 

Travaux :
=========

* Concept, limites et bénéfices du secret.([pdf](/pdfs/Lesecret.pdf))
* Le droit à l'indemnisation : nouveau partage entre solidarité et responsabilité.([pdf](/pdfs/Indemnisation.pdf))
* Le rôle de l'agir dans le processus de subjectivation à l'adolescence.
* Les intellos précaires : essai commenté.([pdf](/pdfs/IntellosPrecaires.pdf))
* Les enjeux éthique du tirage au sort dans l'accès au soin. ([pdf](/pdfs/TirageAuSort.pdf))
* Marie Gaille : La valeur de la vie : essai commenté. ([pdf](/pdfs/LaValeurDeLaVie.pdf))


