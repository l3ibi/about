Title: Ma petite bibliothèque
Tags: lecture
Date: 2017-02-05
Categorie: Blog
Lang: fr
Status: published
Summary: Présentation de ma petite bibliothèque. Une petite selection des quelques livres, écrits que j'ai trouvés importants dans mon petit bout de bonhomme de chemin.



## Disclaimer

Les livres présentés ici, ne sont pas nécessairement liés à des questions de goûts ou de préférences. Ce sont plutôt des livres qui ont modifiés ma façon de voir, qui ont, je pense, eu un impact dans ce que je suis aujourd'hui. 

Je ne les ai pas relus pour l'occasion, les courts résumés ou impressions que j'en donne plus bas correspondent plutôt à mes souvenirs. 

_J'indiquerai lorsque les écrits sont susceptibles de contenir des contenus choquants/déclenchants&nbsp;-->&nbsp;[Trigger warning](http://www.madmoizelle.com/trigger-warnings-171603)_ 


## Insoumission à l'école obligatoire : Catherine Baker

Alors ce bouquin, c'est presque probablement le premier livre que j'ai vraiment lu (je faisais plutôt de la mobylette avec les copains a cette époque). Bref, il faisait beau je "révisais" mon bac en lisant ce livre qui parle en quelque sorte d'école buissonière. 

Ce qui m'a tout de suite plu ce sont les mentions légales "Le photocopiage tue l'industie du livre ... le plus tôt sera le mieux". Dejà on se dit ça commence pas mal. Donc Catherine Baker, dans ce texte, qui est en fait un longue lettre à sa fille, explique pourquoi elle a refusée d'inscrire celle-ci à l'école. Lui préférant l'école de la vie, les rencontres et l'apprentissage par l'expérience. 

[Texte intégrale ](http://tahin-party.org/cbaker.html)

## Guide d'autodéfense numérique : Ouvrage Collectif

C'est un peu grâce à ce guide que j'en suis là, là ici, a écrire des bêtises. Ce livre explique de manière fine et compréhensible, ce qu'il faut savoir lorsqu'on travail avec un ordinateur, notammment en terme de sécurité, privacy etc.

C'est un peu une bible pour moi, c'est le bouquin vers lequel je me réfère lorsque je prépare un atelier ou une présentation sur le chiffrement, la vie privée. 

Il est constitué en deux tomes : un premier sur l'ordinateur hors ligne et un second lorsque l'ordinateur est branché au réseau. 

[Texte intégral ](https://guide.boum.org/)


## Comment j'ai été un violeur : un garçon comme tant d'autre
** Trigger Warning : **  viol  

[Texte intégral](https://lalessiveuse.noblogs.org/files/2015/03/je_ne_veux_plus_etre_un_violeur.pdf)

Ce texte a été aussi une étape importante de mon cheminement intellectuel. C'est le récit d'un mec, qui après avoir définit le viol, se retourne sur ses comportements passés et se rend compte qu'il a pu avoir des comportements violeurs.

C'est un texte qui est probablement une bonne porte d'entrée pour les mecs sur ce qu'est la culture du viol et des comportements que celle-ci induit et valorise. Il est aussi de très bon conseil pour des relations basées sur le consentement et les rapports basé sur la non violence (l'absence de consentement est déjà une violence en soi). 


## Les mots sont des fenêtres ou bien des murs : Marshall B. Rosenberg


Ce livre nous avait été conseillé par un prof en fac de psycho. Il traite de la communication non violente. Il ne me semble pas qu'il théorise particulièrement la chose, il a un aspect plutôt technique, sur concrètement comment on fait. 

En dehors de la question de la non violence, il nous insite à repenser nos modèles de communications dans des contextes de discordance. 

Ce livre m'a été bénéfique en premier lieu dans mes études mais il participe aussi, sans doute à ma manière de communiquer. 


## Après la démocratie : Emmanuel Todd

Honnêtement je sais pas pourquoi je l'ai mis là... mais en fait je crois que c'est parce que j'aime assez bien les analyses et le personnage d'Emmanuel Todd. (je m'étais fait une fiche de lecture mais je ne l'ai pas avec moi). 

La thèse principale et qu'on retrouve a peu près dans tous les travaux de l'auteur, c'est que les systèmes familiaux et leurs origines ont une influence sur la politique générale des pays. En plus de la structures des systèmes familiaux, dans ses essais notamment sur la france, il utilise moins le système familiale que des outils démographiques pour expliquer les changements et les processus qui s'opère. 

Ce bouquin est assez court et facile à lire.  Il n'est pas essentiel mais je l'avais trouvé, à l'époque, plutôt intéressant. 


## En finir avec Eddie Belleguele : Édouard Louis
** Trigger Warning :** violences, sévices, homophobie, alcool.

Ce livre le premier roman (y'en a pas beaucoup dans ma liste), d'Edouard Louis. Il y'raconte le long chemins qu'il a parcouru depuis une banlieue ouvrière du nord de la france, où les seuls amusements lorsqu'on sort de l'usine c'est d'aller se soûler et se bagarrer, jusqu'à sa vie dans une ville. 

Un peu à la manière de Retour à Reims de Didier Eribon (sauf erreur ils sont copains), c'est le vilain petit canard qui s'avère être un cygne, la difficulté d'être différent à l'intérieur de son milieu. 

## Vrac 

* [Manifeste d'une femme trans](http://tahin-party.org/serano.html) de _Julia Serano_
* [Alexandre Jacob, journal d’un anarchiste cambrioleur](http://editions-sarbacane.com/alexandre-jacob-journal-dun-anarchiste-cambrioleur-2/) de _Vincent Henry & Gaël Henry_
* [L'insurrection qui vient en accès libre)](http://www.lafabrique.fr/spip/IMG/pdf_Insurrection.pdf) du _comité invisible_
* [Bitch Planet](http://www.glenatbd.com/bd/bitch-planet-tome-1-9782344014653.htm) de _Kelly Sue DeConninck_ et _Valentine De Landro_
* [Retour à Reims](http://www.fayard.fr/retour-reims-9782213638348) de _Didier Eribon_
* [Constellations](https://constellations.boum.org/spip.php?rubrique1) par le _collectif mauvaise troupe_
* [Face à la Police face à La justice](http://www.syllepse.net/lng_FR_srub_25_iprod_657-face-a-la-police-face-a-la-justice.html) par _Escondida Elie_, _CADECOL Collectif_, _Timélos Dante_
* [Domination policière](http://www.lafabrique.fr/catalogue.php?idArt=729) de _Mathieu Rigouste_
* [Heureux les Heureux](http://www.babelio.com/livres/Reza-Heureux-les-heureux/442168)  de _Yasmina Reza_
* [Commando culotte](http://www.mirionmalle.com/) de _Mirion Mall_ (il y'a une BD en "dure" qui est sortie [ici](http://www.label619.com/fr/news/commando-culotte-dessous-genre-pop-culture))






