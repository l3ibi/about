#!/usr/bin/env python
# -*- coding: utf-8 -*- #
from __future__ import unicode_literals

AUTHOR = u'Camille'
SITENAME = u'About'
SITEURL = 'localhost:8000'
TIMEZONE = 'Europe/ Paris'
DEFAULT_DATE = 'fs'
DATE_FORMATS = {
#    'en': ('en_US','%a, %d %b %Y'),
    'fr': ('fr_FR','%A %d %B %Y'),
}





THEME ='themes/crowsfoot2'

# Folders 
PATH= 'content'
OUTPUT_PATH = 'public'
STATIC_PATHS= [ 'pdfs', 'images','blog','.htaccess',]
PAGES_PATHS=['pages',]
ARTICLE_PATHS=['blog']
CATEGORIES_SAVE_AS = 'categories.html'

# PlUGINS
#PLUGIN_PATHS =['pelican-plugins',]
#PLUGINS = ['pelican_comment_system',]



# Default published
DEFAULT_METADATA = {
    'status': 'draft',
}


# Zone de la date 
TIMEZONE = 'Europe/Paris'

# Translations
DEFAULT_LANG = u'fr'


MENUITEMS= [
            ('Blog', 'category/blog'),
#            ('Recettes','/category/recettes.html'),
            ('CV','pdfs/Cv.pdf'),
            ('Contact','pages/contact.html'),
]

# Comment
#STATIC_COMMENTS = True
PELICAN_COMMENT_SYSTEM = True
PELICAN_COMMENT_SYSTEM_DIR = 'comments'
PELICAN_COMMENT_SYSTEM_IDENTICON_DATA = ('Camille',)



# Feed generation is usually not desired when developing
#FEED_ALL_ATOM= None
#FEED_RSS= None
#CATEGORY_FEED_ATOM = None
#TRANSLATION_FEED_ATOM = None
#AUTHOR_FEED_ATOM = None
#AUTHOR_FEED_RSS = None

PROFILE_IMAGE_URL= '/images/photo.jpg'

# Blogroll
EMAIL_ADDRESS= 'camille@cametleon.me'
GITLAB_ADDRESS='https://git.laquadrature.net/l3ibi'
TWITTER_ADDRESS='https://twitter.com/l3ibi'
MASTODON_ADDRESS='https://toot.aquilenet.fr/@l3ibi'


DEFAULT_PAGINATION = 10

# Uncomment following line if you want document-relative URLs when developing
RELATIVE_URLS = True

#LICENSE
LICENSE_NAME = "CC BY-NC"
LICENSE_URL = "https://creativecommons.org/licenses/by-nc/4.0/"

# ADD bitcoin address on footpage
BITCOIN = "1FTo7mXj2Bfskq6MjDwMeTeUBMxbU1JTzn"
BITCOIN_ADDRESS = "bitcoin:1FTo7mXj2Bfskq6MjDwMeTeUBMxbU1JTzn?message=Blog&time=1509274470"

#Add duniter
DUNITER_ADDRESS ="https://g1.duniter.fr/#/app/wot/BkB7LpTDn5RA4xektrTNA1BLaPYhEPh5PJ5hS9kfeRsh/l3ibi"
DUNITER = "BkB7LpTDn5RA4xektrTNA1BLaPYhEPh5PJ5hS9kfeRsh"

