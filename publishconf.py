#!/usr/bin/env python
# -*- coding: utf-8 -*- #
from __future__ import unicode_literals

# This file is only used if you use `make publish` or
# explicitly specify it as your config file.

import os
import sys
sys.path.append(os.curdir)
from pelicanconf import *

SITEURL = 'about.cametleon.me'
RELATIVE_URLS = True


#PROFILE_IMAGE_URL= '/images/20240124_102017.jpg'

MENUITEMS= [
            ('Blog','/category/blog.html'),
            ('CV','/pdfs/Cv.pdf'),
            ('Contact','/pages/contact.html'),
]

#FEED_ALL_ATOM = 'feeds/all.atom.xml'
#CATEGORY_FEED_ATOM = 'feeds/%s.atom.xml'
#FEED_RSS= 'rss/all.rss.xml'

DELETE_OUTPUT_DIRECTORY = True

# Following items are often useful when publishing

#DISQUS_SITENAME = ""
#GOOGLE_ANALYTICS = ""
