#!/usr/bin/env python
# -*- coding: utf-8 -*- #
from __future__ import unicode_literals

AUTHOR = 'Camille'
SITENAME = 'About'
SITEURL = 'localhost:8000/about'
TIMEZONE = 'Europe/ Paris'
DEFAULT_DATE = 'fs'
#DATE_FORMATS = {
#    'en': ('en_US','%a, %d %b %Y'),
#    'fr': ('fr_FR','%A %d %B %Y'),
#}




THEME = 'themes/crowsfoot2'

# Folders 
PATH= 'content'
STATIC_PATHS= ['pages','pdfs', 'images','blog','.htaccess','keybase.txt']
PAGES_PATHS= 'pages'
ARTICLE_PATHS=['blog','recettes']
INDEX_SAVE_AS = 'index.html' 
CATEGORIES_SAVE_AS = 'categories.html'

# PlUGINS
PLUGIN_PATHS =['pelican-plugins',]
PLUGINS = ['simple_footnotes']


# Default published
#DEFAULT_METADATA = {
#    'Status': 'draft',
#}


# Zone de la date 
TIMEZONE = 'Europe/Paris'

# Translations
DEFAULT_LANG = 'fr'

MENUITEMS= [
            ('Blog','/category/blog.html'),
            ('CV','/pdfs/Cv.pdf'),
            ('Contact','/pages/contact.html'),
]

# Comment
#STATIC_COMMENTS = True
#PELICAN_COMMENT_SYSTEM = True
#PELICAN_COMMENT_SYSTEM_DIR = 'comments'
#PELICAN_COMMENT_SYSTEM_IDENTICON_DATA = ('Camille',)



# Feed generation is usually not desired when developing
#FEED_ALL_ATOM= None
#FEED_RSS= 'rss/all.rss.xml'
#CATEGORY_FEED_ATOM = 'Blog'
#TRANSLATION_FEED_ATOM = None
#AUTHOR_FEED_ATOM = None
#AUTHOR_FEED_RSS = None

PROFILE_IMAGE_URL= '/images/photo.jpg'

# Blogroll
EMAIL_ADDRESS= 'camille@cametleon.me'
GITLAB_ADDRESS='https://framagit.org/l3ibi'



DEFAULT_PAGINATION = 10

# Uncomment following line if you want document-relative URLs when developing
RELATIVE_URLS = True

#LICENSE
LICENSE_NAME = "CC BY-NC"
LICENSE_URL = "https://creativecommons.org/licenses/by-nc/4.0/"

# ADD bitcoin address on footpage
# 112fLyFnHcfuC8btFu9QVsJL6epinpgyVj
